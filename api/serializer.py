
from rest_framework.serializers import ModelSerializer, HyperlinkedModelSerializer
from restaurant_app.models import Client, Waiter, Product, Table, Order, Invoice
from django.contrib.auth.models import User

class UserSerializer(HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ('url', 'username', 'email', 'is_staff')

class ClientSerializer(ModelSerializer):
    class Meta:
        model = Client
        fields = ('first_name', 'last_name', 'observation')

class WaiterSerializer(ModelSerializer):
    class Meta:
        model = Waiter
        fields = ('first_name', 'last_name_1', 'last_name_2')

class ProductSerializer(ModelSerializer):
    class Meta:
        model = Product
        fields = ('name', 'amount', 'type')

class TableSerializer(ModelSerializer):
    class Meta:
        model = Table
        fields = ('number_diner', 'location')

class OrderSerializer(ModelSerializer):
    class Meta:
        model = Order
        fields = ('id_product', 'quantity')

class InvoiceSerializer(ModelSerializer):
    class Meta:
        model = Invoice
        fields = ('date_invoice', 'client_id', 'waiter_id', 'table_id', 'order_id')