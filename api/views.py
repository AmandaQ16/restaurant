from django.contrib.auth.models import User
from restaurant_app.models import Client, Waiter, Product, Table, Order, Invoice
from .serializer import UserSerializer, ClientSerializer, WaiterSerializer, ProductSerializer, TableSerializer, OrderSerializer, InvoiceSerializer

from  rest_framework import viewsets

class UserViewSet(viewsets.ModelViewSet):
    serializer_class = UserSerializer
    queryset = User.objects.all()

class ClientViewSet(viewsets.ModelViewSet):
    serializer_class = ClientSerializer
    queryset = Client.objects.all()

class WaiterViewSet(viewsets.ModelViewSet):
    serializer_class = WaiterSerializer
    queryset = Waiter.objects.all()

class ProductViewSet(viewsets.ModelViewSet):
    serializer_class = ProductSerializer
    queryset = Product.objects.all()

class TableViewSet(viewsets.ModelViewSet):
    serializer_class = TableSerializer
    queryset = Table.objects.all()

class OrderViewSet(viewsets.ModelViewSet):
    serializer_class = OrderSerializer
    queryset = Order.objects.all()

class InvoiceViewSet(viewsets.ModelViewSet):
    serializer_class = InvoiceSerializer
    queryset = Invoice.objects.all()