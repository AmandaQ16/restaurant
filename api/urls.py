from django.urls import path
from rest_framework import routers
from .views import UserViewSet, ClientViewSet, WaiterViewSet, ProductViewSet, TableViewSet, OrderViewSet, InvoiceViewSet

router = routers.DefaultRouter()
router.register(r'user', UserViewSet)
router.register(r'client', ClientViewSet)
router.register(r'waiter', WaiterViewSet)
router.register(r'product', ProductViewSet)
router.register(r'table', TableViewSet)
router.register(r'order', OrderViewSet)
router.register(r'invoice', InvoiceViewSet)


