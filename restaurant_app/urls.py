from django.urls import path
from .views import ListClient, CreateClient, UpdateClient, DeleteClient, ListWaiter, CreateWaiter, UpdateWaiter, \
    DeleteWaiter, ListProduct, CreateProduct, UpdateProduct, DeleteProduct, ListTable, CreateTable, UpdateTable, \
    DeleteTable, ListOrder, CreateOrder, UpdateOrder, DeleteOrder, ListInvoice, CreateInvoice, UpdateInvoice, \
    DeleteInvoice, home

urlpatterns = [
    path('', home, name="home"),

    path('client', ListClient.as_view(), name='client'),
    path('client_form', CreateClient.as_view(), name='create_client'),
    path('update_client/<int:pk>', UpdateClient.as_view(), name='update_client'),
    path('delete_client/<int:pk>', DeleteClient.as_view(), name='delete_client'),

    path('waiter', ListWaiter.as_view(), name='waiter'),
    path('waiter_form', CreateWaiter.as_view(), name='create_waiter'),
    path('update_waiter/<int:pk>', UpdateWaiter.as_view(), name='update_waiter'),
    path('delete_waiter/<int:pk>', DeleteWaiter.as_view(), name='delete_waiter'),

    path('product', ListProduct.as_view(), name='product'),
    path('product_form', CreateProduct.as_view(), name='create_product'),
    path('update_product/<int:pk>', UpdateProduct.as_view(), name='update_product'),
    path('delete_product/<int:pk>', DeleteProduct.as_view(), name='delete_product'),

    path('table', ListTable.as_view(), name='table'),
    path('table_form', CreateTable.as_view(), name='create_table'),
    path('update_table/<int:pk>', UpdateTable.as_view(), name='update_table'),
    path('delete_table/<int:pk>', DeleteTable.as_view(), name='delete_table'),

    path('order', ListOrder.as_view(), name='order'),
    path('order_form', CreateOrder.as_view(), name='create_order'),
    path('update_order/<int:pk>', UpdateOrder.as_view(), name='update_order'),
    path('delete_order/<int:pk>', DeleteOrder.as_view(), name='delete_order'),

    path('invoice', ListInvoice.as_view(), name='invoice'),
    path('invoice_form', CreateInvoice.as_view(), name='create_invoice'),
    path('update_invoice/<int:pk>', UpdateInvoice.as_view(), name='update_invoice'),
    path('delete_invoice/<int:pk>', DeleteInvoice.as_view(), name='delete_invoice')
]
