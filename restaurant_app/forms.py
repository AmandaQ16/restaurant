from django import forms
from .models import Client, Product, Waiter, Table, Order, Invoice


class ClientForm(forms.ModelForm):
    first_name = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control', 'style':'background: #0c0b09; border-color: #625b4b; font-size: 14px; color: white; box-shadow: none;'}))
    last_name = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control', 'style':'background: #0c0b09; border-color: #625b4b; font-size: 14px; color: white; box-shadow: none;'}))
    observation = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control', 'style':'background: #0c0b09; border-color: #625b4b; font-size: 14px; color: white; box-shadow: none;'}))

    class Meta:
        model = Client
        fields = ('first_name', 'last_name', 'observation')

class WaiterForm(forms.ModelForm):
    first_name = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control', 'style':'background: #0c0b09; border-color: #625b4b; font-size: 14px; color: white; box-shadow: none;'}))
    last_name_1 = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control', 'style':'background: #0c0b09; border-color: #625b4b; font-size: 14px; color: white; box-shadow: none;'}))
    last_name_2 = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control', 'style':'background: #0c0b09; border-color: #625b4b; font-size: 14px; color: white; box-shadow: none;'}))

    class Meta:
        model = Waiter
        fields = ('first_name', 'last_name_1', 'last_name_2')

class ProductForm(forms.ModelForm):
    name = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control', 'style':'background: #0c0b09; border-color: #625b4b; font-size: 14px; color: white; box-shadow: none;'}))
    amount = forms.IntegerField(widget=forms.NumberInput(attrs={'class':'form-control', 'style':'background: #0c0b09; border-color: #625b4b; font-size: 14px; color: white; box-shadow: none;'}))
    product_type = (
        ('Drink', 'Drink'),
        ('Source', 'Source'),
    )
    type = forms.ChoiceField(choices=product_type)
    type.widget.attrs.update({'class':'form-control', 'style':'background: #0c0b09; border-color: #625b4b; font-size: 14px; color: white; box-shadow: none;'})
    class Meta:
        model = Product
        fields = ('name', 'amount', 'type')

class TableForm(forms.ModelForm):
    number_diner = forms.IntegerField(widget=forms.NumberInput(attrs={'class':'form-control', 'style':'background: #0c0b09; border-color: #625b4b; font-size: 14px; color: white; box-shadow: none;'}))
    location = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control', 'style':'background: #0c0b09; border-color: #625b4b; font-size: 14px; color: white; box-shadow: none;'}))

    class Meta:
        model = Table
        fields = ('number_diner', 'location')

class OrderForm(forms.ModelForm):
    id_product = forms.ModelChoiceField(queryset=Product.objects.all())
    id_product.widget.attrs.update({'class':'form-control', 'style':'background: #0c0b09; border-color: #625b4b; font-size: 14px; color: white; box-shadow: none;'})
    quantity = forms.IntegerField(widget=forms.NumberInput(attrs={'class':'form-control', 'style':'background: #0c0b09; border-color: #625b4b; font-size: 14px; color: white; box-shadow: none;'}))
    class Meta:
        model = Order
        fields = ('id_product', 'quantity')

class InvoiceForm(forms.ModelForm):
    date_invoice = forms.DateField(widget=forms.SelectDateWidget(attrs={'class':'form-control', 'style':'background: #0c0b09; border-color: #625b4b; font-size: 14px; color: white; box-shadow: none;'}))
    client_id = forms.ModelChoiceField(queryset=Client.objects.all())
    client_id.widget.attrs.update({'class':'form-control', 'style':'background: #0c0b09; border-color: #625b4b; font-size: 14px; color: white; box-shadow: none;'})
    waiter_id = forms.ModelChoiceField(queryset=Waiter.objects.all())
    waiter_id.widget.attrs.update({'class':'form-control', 'style':'background: #0c0b09; border-color: #625b4b; font-size: 14px; color: white; box-shadow: none;'})
    table_id = forms.ModelChoiceField(queryset=Table.objects.all())
    table_id.widget.attrs.update({'class':'form-control', 'style':'background: #0c0b09; border-color: #625b4b; font-size: 14px; color: white; box-shadow: none;'})
    order_id = forms.ModelMultipleChoiceField(queryset=Order.objects.all())
    order_id.widget.attrs.update({'class':'form-control', 'style':'background: #0c0b09; border-color: #625b4b; font-size: 14px; color: white; box-shadow: none;'})
    class Meta:
        model = Invoice
        fields = ('date_invoice', 'client_id', 'waiter_id', 'table_id', 'order_id')