from django.contrib import admin
from .models import Client, Product, Waiter, Table, Invoice, Order

# Register your models here.
admin.site.register(Client)
admin.site.register(Product)
admin.site.register(Waiter)
admin.site.register(Table)
admin.site.register(Invoice)
admin.site.register(Order)