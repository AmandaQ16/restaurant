from django.db import models

# Create your models here.

class Client(models.Model):
    first_name = models.CharField(max_length=45)
    last_name = models.CharField(max_length=45)
    observation = models.TextField()
    def __str__(self):
        return f'{self.first_name} {self.last_name}'

class Waiter(models.Model):
    first_name = models.CharField(max_length=45)
    last_name_1 = models.CharField(max_length=45)
    last_name_2 = models.CharField(max_length=45)
    def __str__(self):
        return f'{self.first_name} {self.last_name_1}'

class Product(models.Model):
    name = models.CharField(max_length=45)
    amount = models.IntegerField()
    product_type = (
        ('Drink', 'Drink'),
        ('Source', 'Source'),
    )
    type = models.CharField(max_length=20, choices=product_type)
    def __str__(self):
        return f'{self.name}'

class Table(models.Model):
    number_diner = models.IntegerField()
    location = models.CharField(max_length=45)
    def __str__(self):
        return f'{self.location}'

class Order(models.Model):
    id_product = models.ForeignKey(Product, on_delete=models.CASCADE)
    quantity = models.IntegerField()
    def __str__(self):
        return f'{self.id}'

class Invoice(models.Model):
    date_invoice = models.DateField()
    client_id = models.ForeignKey(Client, on_delete=models.CASCADE)
    waiter_id = models.ForeignKey(Waiter, on_delete=models.CASCADE)
    table_id = models.ForeignKey(Table, on_delete=models.CASCADE)
    order_id = models.ManyToManyField(Order)
    def __str__(self):
        return f'{self.id}'
