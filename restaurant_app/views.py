from django.shortcuts import render

# Create your views here.
from django.views.generic import ListView, CreateView, DeleteView, UpdateView
from .models import Client, Waiter, Product, Table, Order, Invoice
from .forms import ClientForm, WaiterForm, ProductForm, TableForm, OrderForm, InvoiceForm

def home(request):
    return render(request, 'base.html')

# LIST VIEWS
class ListClient(ListView):
    model = Client
    template_name = 'list_client.html'
    queryset = Client.objects.all()
    context_object_name = 'client'

class ListWaiter(ListView):
    model = Waiter
    template_name = 'list_waiter.html'
    queryset = Waiter.objects.all()
    context_object_name = 'waiter'

class ListProduct(ListView):
    model = Product
    template_name = 'list_product.html'
    queryset = Product.objects.all()
    context_object_name = 'product'

class ListTable(ListView):
    model = Table
    template_name = 'list_table.html'
    queryset = Table.objects.all()
    context_object_name = 'table'

class ListOrder(ListView):
    model = Order
    template_name = 'list_order.html'
    queryset = Order.objects.all()
    context_object_name = 'order'

class ListInvoice(ListView):
    model = Invoice
    template_name = 'list_invoice.html'
    queryset = Invoice.objects.all()
    context_object_name = 'invoice'


# CREATE VIEWS
class CreateClient(CreateView):
    model = Client
    template_name = 'add_client.html'
    form_class = ClientForm
    success_url = 'client'

class CreateWaiter(CreateView):
    model = Waiter
    template_name = 'add_waiter.html'
    form_class = WaiterForm
    success_url = 'waiter'

class CreateProduct(CreateView):
    model = Product
    template_name = 'add_product.html'
    form_class = ProductForm
    success_url = 'product'

class CreateTable(CreateView):
    model = Table
    template_name = 'add_table.html'
    form_class = TableForm
    success_url = 'table'

class CreateOrder(CreateView):
    model = Order
    template_name = 'add_order.html'
    form_class = OrderForm
    success_url = 'order'

class CreateInvoice(CreateView):
    model = Invoice
    template_name = 'add_invoice.html'
    form_class = InvoiceForm
    success_url = 'invoice'


# UPDATE VIEWS
class UpdateClient(UpdateView):
    model = Client
    template_name = 'update_client.html'
    form_class = ClientForm
    success_url = '/client'
    pk_url_kwarg = 'pk'

class UpdateWaiter(UpdateView):
    model = Waiter
    template_name = 'update_waiter.html'
    form_class = WaiterForm
    success_url = '/waiter'
    pk_url_kwarg = 'pk'

class UpdateProduct(UpdateView):
    model = Product
    template_name = 'update_product.html'
    form_class = ProductForm
    success_url = '/product'
    pk_url_kwarg = 'pk'

class UpdateTable(UpdateView):
    model = Table
    template_name = 'update_table.html'
    form_class = TableForm
    success_url = '/table'
    pk_url_kwarg = 'pk'

class UpdateOrder(UpdateView):
    model = Order
    template_name = 'update_order.html'
    form_class = OrderForm
    success_url = '/order'
    pk_url_kwarg = 'pk'

class UpdateInvoice(UpdateView):
    model = Invoice
    template_name = 'update_invoice.html'
    form_class = InvoiceForm
    success_url = '/invoice'
    pk_url_kwarg = 'pk'

# DELETE VIEWS
class DeleteClient(DeleteView):
    model = Client
    template_name = 'delete_client.html'
    success_url = '/client'
    pk_url_kwarg = 'pk'

class DeleteWaiter(DeleteView):
    model = Waiter
    template_name = 'delete_waiter.html'
    success_url = '/waiter'
    pk_url_kwarg = 'pk'

class DeleteProduct(DeleteView):
    model = Product
    template_name = 'delete_product.html'
    success_url = '/product'
    pk_url_kwarg = 'pk'

class DeleteTable(DeleteView):
    model = Table
    template_name = 'delete_table.html'
    success_url = '/table'
    pk_url_kwarg = 'pk'

class DeleteOrder(DeleteView):
    model = Order
    template_name = 'delete_order.html'
    success_url = '/order'
    pk_url_kwarg = 'pk'

class DeleteInvoice(DeleteView):
    model = Invoice
    template_name = 'delete_invoice.html'
    success_url = '/invoice'
    pk_url_kwarg = 'pk'