# Generated by Django 4.0.4 on 2022-05-09 22:16

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Client',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('first_name', models.CharField(max_length=45)),
                ('last_name', models.CharField(max_length=45)),
                ('observation', models.TextField()),
            ],
        ),
        migrations.CreateModel(
            name='Product',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=45)),
                ('amount', models.IntegerField()),
                ('type', models.CharField(choices=[('Drink', 'Drink'), ('Source', 'Source')], max_length=20)),
            ],
        ),
        migrations.CreateModel(
            name='Table',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('number_diner', models.IntegerField()),
                ('location', models.CharField(max_length=45)),
            ],
        ),
        migrations.CreateModel(
            name='Waiter',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('first_name', models.CharField(max_length=45)),
                ('last_name_1', models.CharField(max_length=45)),
                ('last_name_2', models.CharField(max_length=45)),
            ],
        ),
        migrations.CreateModel(
            name='Order',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('quantity', models.IntegerField()),
                ('id_product', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='restaurant_app.product')),
            ],
        ),
        migrations.CreateModel(
            name='Invoice',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date_invoice', models.DateField()),
                ('client_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='restaurant_app.client')),
                ('order_id', models.ManyToManyField(to='restaurant_app.order')),
                ('table_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='restaurant_app.table')),
                ('waiter_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='restaurant_app.waiter')),
            ],
        ),
    ]
